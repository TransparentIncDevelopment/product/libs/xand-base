//! Chain-agnostic models of blockchain data and statuses.

pub use super::transaction_id::{TransactionId, TransactionIdError};
pub use super::CidrBlock;

use serde::{Deserialize, Serialize};
/// A chain-implementation agnostic representation of the status of a transaction/extrinsic/whatever
/// that a client has submitted (or tried to submit) to the network.
///
/// From's and to's are implemented for the most common conversion scenarios.
#[derive(Serialize, Deserialize, Debug, EnumString, PartialEq, Eq, Clone, ToString)]
pub enum TransactionStatus {
    /// The chain and/or validator has never seen the transaction (or refuses to admit it has)
    Unknown,
    /// The chain and/or validator has seen the transaction but has not made a decision about it
    Pending,
    /// The chain and/or validator has decided the transaction is invalid. The parameter is the
    /// reason for rejection.
    Invalid(String),
    /// The transaction is successfully committed to the chain
    Committed,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct History {
    // Total count of txn history not just the paged data which is
    // included in transactions.
    pub total: u64,
    pub transactions: Vec<Transaction>,
}

/// The response that the petri transaction history route returns. Pagination information is not yet
/// handled, but when it is, should be included in the response headers, not this thing.
#[derive(Clone, Debug, Serialize, Deserialize, Default)]
#[serde(rename_all = "camelCase")]
pub struct AccountHistoryResp {
    /// Transactions on this page. We simply return raw bytes, the client is expected to
    /// deserialize. In this case, into [UnchecheckedExtrinsic]s
    pub transactions: Vec<Vec<u8>>,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Transaction {
    pub operation: String,
    pub signer_address: String,
    pub transaction_id: String,
    #[serde(flatten)]
    pub txn: XandTransaction,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize, strum_macros::Display)]
/// Chain-agnostic versions of all of our transaction types, wrapped up in an enum that provides
/// conversions and deserialization from the specific transaction types in substrate.
#[serde(tag = "serde_operation")]
pub enum XandTransaction {
    // NOTE: The serialization names should match the actual transaction names in substrate
    #[strum(serialize = "register_account_as_ap")]
    RegAP(RegisterAccountAsAp),
    #[strum(serialize = "register_account_as_kycd")]
    RegKYC(RegisterAccountAsKycd),
    #[strum(serialize = "set_trust_node_id")]
    SetTrust(SetTrustNodeId),
    #[strum(serialize = "set_pending_mint_expire_time")]
    SetPMintExpire(SetPendingMintExpire),
    #[strum(serialize = "transfer")]
    Transfer(Transfer),
    #[strum(serialize = "create_pending_mint")]
    CreatePendingMint(PendingMint),
    #[strum(serialize = "fulfill_pending_mint")]
    FulfillMint(FulfillPendingMint),
    #[strum(serialize = "cancel_pending_mint")]
    CancelMint(CancelPendingMint),
    #[strum(serialize = "smelt")]
    CreatePendingSmelt(PendingSmelt),
    #[strum(serialize = "fulfill_smelt")]
    FulfillSmelt(FulfillPendingSmelt),
    #[strum(serialize = "add_authority_key")]
    AddAuthorityKey(AddAuthorityKey),
    #[strum(serialize = "add_authority_key")]
    RemoveAuthorityKey(RemoveAuthorityKey),
    #[strum(serialize = "whitelist_cidr_block")]
    WhitelistCidrBlock(WhitelistCidrBlock),
    #[strum(serialize = "remove_whitelist_cidr_block")]
    RemoveWhitelistCidrBlock(RemoveWhitelistCidrBlock),
    #[strum(serialize = "root_whitelist_cidr_block")]
    RootWhitelistCidrBlock(RootWhitelistCidrBlock),
    #[strum(serialize = "root_remove_whitelist_cidr_block")]
    RootRemoveWhitelistCidrBlock(RootRemoveWhitelistCidrBlock),
}

/// A transaction and its associated status in chain-agnostic form
/// Returned from `petri_cliient.get_transaction()`
#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
pub struct XandTransactionWithStatus {
    pub transaction: XandTransaction,
    pub status: TransactionStatus,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterAccountAsAp {
    /// Stringified version of the ss58 address to register
    pub address: String,
}

/// NOTE: Currently unused in v1 - for registering client wallets
#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RegisterAccountAsKycd {
    /// Stringified version of the ss58 address to register
    pub address: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SetTrustNodeId {
    /// Stringified version of the ss58 address to become the trust
    pub address: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SetPendingMintExpire {
    /// Value of new timeout in seconds
    pub expire_in_seconds: u64,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Transfer {
    /// Stringified version of the public key funds will be transferred to
    pub destination_account: String,
    /// Amount to transfer in cents
    pub amount_in_minor_unit: u64,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct BankAccountId {
    pub routing_number: String,
    pub account_number: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub enum EncodedBankAccount {
    // this gets added as a member to Mint/Smelt
    Naive(String),
    Encrypted(EncryptedAccountEnvelope),
}

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct EncryptedAccountEnvelope {
    pub ap_public_key: Vec<u8>,
    pub trustee_public_key: Vec<u8>,
    pub encrypted_account: Vec<u8>,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PendingMint {
    /// Amount of the pending mint, in USD cents
    pub amount_in_minor_unit: u64,
    /// A nonce uniquely identifying this pending mint
    pub nonce: Vec<u8>,

    pub account: EncodedBankAccount,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PendingSmelt {
    /// Amount of the pending smelt, in USD cents
    pub amount_in_minor_unit: u64,
    /// A nonce uniquely identifying this pending smelt
    pub nonce: Vec<u8>,
    /// Encrypted information about the bank account that the trust should be depositing funds to
    pub account: EncodedBankAccount,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FulfillPendingSmelt {
    /// Nonce of the smelt to mark as fulfilled
    pub nonce: Vec<u8>,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FulfillPendingMint {
    /// Nonce of the mint to mark as fulfilled
    pub nonce: Vec<u8>,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CancelPendingMint {
    /// Nonce of the mint to mark as fulfilled
    pub nonce: Vec<u8>,
    /// The reason for the cancellation
    pub reason: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AddAuthorityKey {
    /// Stringified version of the validator's signing pubkey (ss58 address from sr25519 pubkey)
    pub account_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RemoveAuthorityKey {
    /// Stringified version of the validator's signing pubkey (ss58 address from sr25519 pubkey)
    pub account_id: String,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct WhitelistCidrBlock {
    pub cidr_block: CidrBlock,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RemoveWhitelistCidrBlock {
    pub cidr_block: CidrBlock,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RootWhitelistCidrBlock {
    pub account: String,
    pub cidr_block: CidrBlock,
}

#[derive(Clone, Debug, PartialEq, Eq, Constructor, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct RootRemoveWhitelistCidrBlock {
    pub account: String,
    pub cidr_block: CidrBlock,
}

/// Hex encodes given nonce
fn nonce_to_string(nonce: &[u8]) -> String {
    /*
        This logic needs to match what's in the AP API.
        I made the conscious choice NOT to create a Nonce struct
        to contain ths logic since we're about to get rid of
        Nonce's altogether.
    */
    bs58::encode(&nonce).into_string()
}

/// Utility trait to stringify nonce values
pub trait HasNonce {
    fn get_nonce(&self) -> &Vec<u8>;

    fn nonce_to_string(&self) -> String {
        nonce_to_string(self.get_nonce())
    }
}

impl HasNonce for PendingMint {
    fn get_nonce(&self) -> &Vec<u8> {
        &self.nonce
    }
}

impl HasNonce for FulfillPendingMint {
    fn get_nonce(&self) -> &Vec<u8> {
        &self.nonce
    }
}

impl HasNonce for CancelPendingMint {
    fn get_nonce(&self) -> &Vec<u8> {
        &self.nonce
    }
}

impl HasNonce for PendingSmelt {
    fn get_nonce(&self) -> &Vec<u8> {
        &self.nonce
    }
}

impl HasNonce for FulfillPendingSmelt {
    fn get_nonce(&self) -> &Vec<u8> {
        &self.nonce
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::str::FromStr;

    #[test]
    pub fn transaction_status_from_str() {
        let status_str = TransactionStatus::Unknown.to_string();

        let result = TransactionStatus::from_str(&status_str).unwrap();

        assert_eq!(result.to_string(), status_str);
    }
}
