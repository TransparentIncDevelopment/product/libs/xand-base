//! Unix specific code for process cleanup

use nix::sys::signal::Signal;
use nix::unistd::Pid;
use std::os::unix::process::CommandExt;
use std::process::{Child, Command};

pub struct KillChildOnDrop {
    pub command: Command,
    child: Option<Child>,
}

impl KillChildOnDrop {
    pub fn new(command: Command) -> Self {
        KillChildOnDrop {
            command,
            child: None,
        }
    }

    pub fn spawn(&mut self) -> std::io::Result<&Self> {
        self.child = Some(
            // Spoooooky -- setsid() is unsafe. Yarn in particular has forced our hand here by
            // being absolutely crap at forwarding signals to children, but it is useful for
            // any processes that might "go rouge"
            unsafe {
                self.command
                    .pre_exec(|| {
                        // Force the processes to run in a new process group so they can be murdered
                        nix::unistd::setsid().unwrap();
                        Ok(())
                    })
                    .spawn()?
            },
        );
        Ok(self)
    }

    pub fn kill(&mut self) {
        self.child.as_mut().map(|c| c.kill());
    }
}

impl Drop for KillChildOnDrop {
    fn drop(&mut self) {
        if let Some(ref mut child) = &mut self.child {
            let pgroup = nix::unistd::getpgid(Some(Pid::from_raw(child.id() as i32))).unwrap();
            let _ = nix::sys::signal::killpg(pgroup, Signal::SIGTERM);
            // Sigterm doesn't always seem to work for xandstrate
            let _ = child.kill();
            // If wait isn't called we can leave zombie processes behind
            let _ = child.wait();
        }
    }
}
